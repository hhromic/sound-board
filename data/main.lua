function sol.main:on_started()
  sol.audio.preload_sounds()
end

function sol.main:on_key_pressed(key, modifiers)
  local handled = false
  if key == "f11" or
    (key == "return" and (modifiers.alt or modifiers.control)) then
    sol.video.set_fullscreen(not sol.video.is_fullscreen())
    handled = true
  elseif key == "f4" and modifiers.alt then
    sol.main.exit()
    handled = true
  elseif key == "a" then
    sol.audio.stop_music()
    sol.audio.play_music("sm64_slider")
    handled = true
  elseif key == "s" then
    sol.audio.stop_music()
    sol.audio.play_music("title")
    handled = true
  elseif key == "d" then
    sol.audio.stop_music()
    sol.audio.play_music("zelda")
    handled = true
  elseif key == "z" then
    sol.audio.play_sound("treasure")
    handled = true
  elseif key == "x" then
    sol.audio.play_sound("victory")
    handled = true
  end

  return handled
end
